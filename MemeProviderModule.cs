using System;
using System.Collections.Generic;
using System.Text;
using Discord;
using Discord.Commands;
using System.Threading.Tasks;
using System.Net.Http;
using Lithiumbot;

namespace Lithiumbot {
    public class MemeProviderModule : ModuleBase {
        static HttpClient client_ = new HttpClient();


        const string INSPIROBOT_API_URL = "http://inspirobot.me/api?generate=true";
        const string INSPIROBOT_WEBSITE = "http://www.inspirobot.me/";
        const string INSPIROBOT_ICON = "http://inspirobot.me/website/images/inspirobot-dark-green.png";
        string[] INSPIRATIONAL_WORDS = {
            "Truly inspiring.",
            "Life-changing.",
            "Makes you think.",
            "Think about that...",
            "Everything makes sense now.",
            "Mind-blowing!",
            "Unimaginably truthful.",
            "The unbridled Truth.",
            "🤔"
        };

        [Command("inspire"), Summary("Grab an inspirational image from the Inspirobot API.")]
        public async Task Inspire() {
            string img = await client_.GetStringAsync(INSPIROBOT_API_URL);
            var embed = new EmbedBuilder();
            embed.ImageUrl = img;
            embed.Title = INSPIRATIONAL_WORDS.SelectRandom();
            embed.Author = new EmbedAuthorBuilder().WithName("Inspirobot").WithUrl(INSPIROBOT_WEBSITE).WithIconUrl(INSPIROBOT_ICON);
            await Context.Channel.SendMessageAsync(String.Empty, embed: embed);
        }

        [Command("play")]
        public Task CleanPlayCommand([Remainder] string args = "") {
            Task.Run(async () => {
                await Task.Delay(Config.Music.DeletionDelay);
                await Context.Message.DeleteAsync();
            });
            return Task.CompletedTask;
        }
    }
}
