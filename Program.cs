﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using System.Threading.Tasks;


namespace Lithiumbot {
    class Program {

        DiscordSocketClient client = new DiscordSocketClient();
        CommandService commands = new CommandService();
        IServiceProvider services = new ServiceCollection().BuildServiceProvider();

        static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync() {
            try {
                Config.Load();
            } catch (System.IO.FileNotFoundException e) {
                Console.WriteLine($"An error occured: {e}");
                throw;
            }

            client.Log += Log;

            await InstallCommands();

            await client.LoginAsync(TokenType.Bot, Config.Discord.Token);
            await client.StartAsync();

            await Task.Delay(-1);
        }


        private Task Log(LogMessage msg) {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        private async Task InstallCommands() {
            client.MessageReceived += HandleCommand;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        private async Task HandleCommand(SocketMessage messageParam) {
            var message = messageParam as SocketUserMessage;
            if (message == null) return;
            int argPos = 0;
            
            if (!(message.HasCharPrefix('§', ref argPos)
                || message.HasStringPrefix("$play", ref argPos)
                || message.HasMentionPrefix(client.CurrentUser, ref argPos)))
                return;
            if (message.Content.StartsWith('$'))
                message.HasCharPrefix('$', ref argPos);
            var context = new CommandContext(client, message);

            var result = await commands.ExecuteAsync(context, argPos, services);
            if (!result.IsSuccess) {
                Console.WriteLine($"Da heck, command borked: {message.Content}");
            }
        }

    }
}
