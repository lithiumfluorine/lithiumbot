﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lithiumbot {
    static class RandomUtil {
        static Random random_ = new Random();
        public static T SelectRandom<T>(this T[] arr) {
            return arr[random_.Next(0, arr.Length)];
        }
    }
}
