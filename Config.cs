﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;

namespace Lithiumbot {
    public class Config {
        private const string CONFIG_PATH = "./config.json";
        private static Config _instance = new Config();

        public static void Load() {
            if (!File.Exists(CONFIG_PATH)) {
                throw new FileNotFoundException($"Missing config file at '{CONFIG_PATH}'.");
            }
            _instance = JsonConvert.DeserializeObject<Config>(File.ReadAllText(CONFIG_PATH));
        }

        public class DiscordSettings {
            [JsonProperty("token")]
            public string Token;
        }

        public class MusicSettings {
            [JsonProperty("deletionDelay")]
            public int DeletionDelay = 15000;
        }


        [JsonProperty("discord")]
        DiscordSettings _discord { get; set; } = new DiscordSettings();

        [JsonProperty("music")]
        MusicSettings _music { get; set; } = new MusicSettings();

        public static DiscordSettings Discord => _instance._discord;
        public static MusicSettings Music => _instance._music;

    }
}
